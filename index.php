<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <title>Coffee Shop</title>
        <!-- Font Awesome icons (free version)-->
        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        <script src="https://use.fontawesome.com/releases/v5.15.1/js/all.js" crossorigin="anonymous"></script>
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="./assets/css/styles.css" rel="stylesheet" />
    </head>
    <body id="page-top">
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
            <div class="container">
                <a class="navbar-brand js-scroll-trigger" href="#page-top"><img src="assets/icon/logo.svg" alt="" /></a>
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    Menu
                    <i class="fas fa-bars ml-1"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav text-uppercase ml-auto">
                        <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#">About</a></li>
                        <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#menus">Menu</a></li>
                        <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#">Moods</a></li>
                        <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#blog">Blogs</a></li>
                        <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#">Contact</a></li>
                        <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#"><i class="fa fa-search"></i></a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Masthead-->
        <header class="masthead">
            <div class="container">
                <div data-aos="fade-down" class="masthead-heading">Life begins after Coffee.</div>
                <a class="btn btn-primary btn-md text-uppercase js-scroll-trigger" href="#menus">View Menu</a>
            </div>
        </header>
        <!-- Menus-->
        <section id="menus">
            <div class="container">
                <div data-aos="fade-right" class="text-center">
                    <h1 class="section-heading">What would you like to have?</h1>
                    <p class="section-subheading">Coffee plunger pot sweet barista, grounds acerbic coffee instant crema cream in half and half. Spoon lungovariety, as, siphon, ristretto, iced brewed and acerbic affogato grinder.</p>
                </div>
                <div data-aos="fade-left" class="row text-center">
                    <?php
                    $data = file_get_contents("./menus.json");
                    $menus = json_decode($data, true);
                    foreach($menus as $menu){ 
                        ?>
                        <div class="col-md-3">
                            <img src="<?= $menu['img'] ?>" class="img-fluid" alt="<?= $menu['name']?>">
                            <div class="title"><?= $menu['name']?></div>
                        </div>
                    <?php }
                    ?>
                </div>
            </div>
        </section>
        <section id="lead">
            <div data-aos="fade-down" class="container">
                <div class="text-center">
                    <h1 class="section-heading ">Extraction instant that variety  white robusta strong</h1>
                    <p class="section-subheading">Coffee plunger pot sweet barista, grounds acerbic coffee instant crema cream in half and half. Spoon lungovariety, as, siphon, ristretto, iced brewed and acerbic affogato grinder. Mazagran café au lait wings spoon,percolator milk latte dark strong. Whipped, filter latte, filter aromatic grounds doppio caramelization half and half</p>
                    <a class="btn btn-primary btn-md text-uppercase js-scroll-trigger" href="#contact">Contact Us</a>
                </div>
            </div>
        </section>
        <section id="benefits" class="bg-secondary">
            <div class="container">
                <div class="text-center">
                    <h2 class="section-heading">Health Benefits of Coffee</h2>
                    <div data-aos="fade-up" class="row d-ruby">
                        <div class="col-md-4">
                            <img src="./assets/icon/battery-full.svg" class="img-fluid" alt="Boost">
                            <p class="benefit">BOOST ENERGY LEVEL</p>
                        </div>
                        <div class="col-md-4">
                            <img src="./assets/icon/sun.svg" class="img-fluid" alt="Sun">
                            <p class="benefit">REDUCE DEPRESSION</p>
                        </div>
                        <div class="col-md-4">
                            <img src="./assets/icon/weight.svg" class="img-fluid" alt="Weight">
                            <p class="benefit">AID IN WEIGHT LOSS</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="blog">
            <div data-aos="fade-down" class="container">
                <div class="row">
                    <div class="col">
                        <img src="./assets/img/blog_1.jpg" class="img-fluid" alt="Blog coffee">
                    </div>
                    <div class="col">
                        <h3 class="text-uppercase">blog</h3>
                        <h1>Qui espresso, grounds to go</h1>
                        <p>December 12, 2019 | Espresso</p>
                            <p>Skinny caffeine aged variety filter saucer redeye, sugarsit steamed eu extraction organic. Beans, crema halfand half fair trade carajillo in a variety dripper doppiopumpkin spice cup lungo, doppio, est trifecta breve and,rich,extraction robusta a eu instant. Body sugarsteamed,aftertaste, decaffeinated coffee fair trade sit,white shop fair trade galão, dark crema breve frappuccino iced strong siphon trifecta in a at viennese</p>
                        <a class="btn btn-outline-dark" href="#">READ MORE <i class="fa fa-arrow-right"></i></a>
                    </div>
                </div>
            </div>
        </section>
        <!-- Footer-->
        <footer class="footer py-4">
            <div class="container">
                <div class="row align-items-center webkit">
                    <div class="col-lg-3 text-lg-center"><img src="./assets/icon/logo.svg" class="img-fluid" alt="Coffe"></div>
                    <div class="col-lg-4 text-lg-left">
                        <p>2800 S White Mountain Rd | Show Low AZ 85901</p>
                        <p>(928) 537-1425 | info@grinder-coffee.com</p>
                        <a class="btn btn-dark btn-social mx-2" href="#!"><i class="fab fa-instagram"></i></a>
                        <a class="btn btn-dark btn-social mx-2" href="#!"><i class="fab fa-facebook-f"></i></a>
                    </div>
                    <div class="col-lg-5 text-lg-left">
                        <form>
                            <label for="newsletter">NEWSLETTER</label> <br>
                            <div class="form-inline">
                                <input type="text" placeholder="YOUR EMAIL ADDRESS"> <input class="btn btn-primary btn-sm" type="submit" value="SUBSCRIBE">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Bootstrap core JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
        <script src="./assets/js/scripts.js"></script>
    </body>
</html>
